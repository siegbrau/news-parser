FROM openjdk:11-jre
ADD  /target/newsparser-0.0.1-SNAPSHOT.jar newsparser-0.0.1-SNAPSHOT.jar
EXPOSE 8080
ENTRYPOINT  ["java","-jar","newsparser-0.0.1-SNAPSHOT.jar"]
