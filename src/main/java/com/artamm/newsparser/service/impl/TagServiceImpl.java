package com.artamm.newsparser.service.impl;

import com.artamm.newsparser.model.Tag;
import com.artamm.newsparser.service.TagService;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class TagServiceImpl implements TagService {

    public Tag[] allTagList() {
        return Tag.values();
    }

    @Override
    public boolean tagExists(String required) {
        for (Tag tag : Tag.values()
        ) {
            if (tag.toString().equalsIgnoreCase(required)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public List<Tag> defineTags(String text) {
        List<Tag> tagList = new ArrayList<>();

        if (text.contains("игр") || text.contains("game")) {
            tagList.add(Tag.games);
        }

        if (text.contains("разраб") || text.contains("dev")) {
            tagList.add(Tag.dev);
        }

        if (text.contains("технолог")) {
            tagList.add(Tag.technology);
        }
        return tagList;
    }

}
