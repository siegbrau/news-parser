package com.artamm.newsparser.service.impl;

import com.artamm.newsparser.model.Source;
import com.artamm.newsparser.model.jNews;
import com.artamm.newsparser.service.ParseNews;
import com.artamm.newsparser.service.TagService;
import com.artamm.newsparser.service.jNewsService;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Component
public class ParseNewsImpl implements ParseNews {

    private final jNewsService jNewsService;
    private final TagService tagService;

    @Autowired
    public ParseNewsImpl(com.artamm.newsparser.service.jNewsService jNewsService,
                         TagService tagService) {
        this.jNewsService = jNewsService;
        this.tagService = tagService;
    }
    //    TODO сделать мультитрединг https://dzone.com/articles/multi-threading-in-spring-boot-using-completablefu
    //    TODO TOKEN

    @Scheduled(fixedRate = 30000)
    public void parseAllNewNews() {

        parseFromSource(Source.OTHER, "https://news.ycombinator.com/", "storylink");
        parseFromSource(Source.STOPGAME, "https://stopgame.ru/news", "item article-summary");
        parseFromSource(Source.IXBT, "https://gametech.ru",
                "d-flex  col-sm-6  col-lg-3 col-md-4 col-12 align-items-strech  pb-3");
        //TODO DTF TJ VC parser

        /* DTF, TJ, VC have the same developers */
        //        parseFromSource(Source.DTF, "https://dtf.ru/",
        //                "content-title content-title--short l-island-a");
        //        parseFromSource(Source.VC, "https://vc.ru/",
        //                "content-title content-title--short l-island-a");
        //        parseFromSource(Source.TJ, "https://tjournal.ru/",
        //                "content-title content-title--short l-island-a");
    }

    private void parseFromSource(Source source, String fixedUrl, String searchClass) {

        try {
            Document doc = Jsoup.connect(fixedUrl)
                                .userAgent("Chrome")
                                .timeout(10000)
                                .referrer("https://www.duckduckgo.com/")
                                .get();
            Elements news = doc.getElementsByClass(searchClass);
            for (Element el : news
            ) {
                if (!jNewsService.isExist(el.ownText())) {
                    Element url = el.select("a").first();
                    String href = url.attr("abs:href");
                    //                    Only title from STOPGAME elements
                    String text = source.equals(Source.STOPGAME) ?
                            el.getElementsByClass("caption caption-bold").get(0).text() :
                            el.text();
                    jNewsService.save(new jNews(text.toUpperCase(), href, source,
                            tagService.defineTags(text.toLowerCase())));
                }

            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
