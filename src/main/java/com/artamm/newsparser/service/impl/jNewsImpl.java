package com.artamm.newsparser.service.impl;

import com.artamm.newsparser.model.Source;
import com.artamm.newsparser.model.Tag;
import com.artamm.newsparser.model.jNews;
import com.artamm.newsparser.service.repo.jNewsRepository;
import com.artamm.newsparser.service.jNewsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class jNewsImpl implements jNewsService {
    private final jNewsRepository jNewsRepository;

    @Autowired
    public jNewsImpl(jNewsRepository jNewsRepository) {
        this.jNewsRepository = jNewsRepository;
    }

    @Override
    public List<jNews> getAllNews() {
        return jNewsRepository.findAll();
    }

    @Override
    public List<jNews> getAllBySource(String source) {
        return jNewsRepository.findAllBySource(Source.valueOf(source.toUpperCase()));
    }

    @Override
    public List<jNews> getAllByTitle(String title) {
        return jNewsRepository.findAllByTitleContainingIgnoreCase(title);
    }

    @Override
    public List<jNews> getAllByTag(String tag) {
        if (List.of(Tag.values()).toString().contains(tag)) {
            return jNewsRepository.findAllByTagListContainingIgnoreCase(Tag.valueOf(tag));
        } else {
            return jNewsRepository.findAll();
        }
    }

    @Override
    public void save(jNews news) {
        jNewsRepository.save(news);
    }

    @Override
    public boolean isExist(String title) {
        return jNewsRepository.existsByTitle(title);
    }
}
