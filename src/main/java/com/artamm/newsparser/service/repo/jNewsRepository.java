package com.artamm.newsparser.service.repo;

import com.artamm.newsparser.model.Source;
import com.artamm.newsparser.model.Tag;
import com.artamm.newsparser.model.jNews;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.CrossOrigin;

import java.util.List;

@Repository
@CrossOrigin(origins = "*", allowedHeaders = "*")
public interface jNewsRepository extends JpaRepository<jNews, Long> {
    boolean existsByTitle(String title);

    List<jNews> findAllBySource(Source source);

    List<jNews> findAllByTitleContainingIgnoreCase(String title);

    List<jNews> findAllByTagListContainingIgnoreCase(Tag tag);
}
