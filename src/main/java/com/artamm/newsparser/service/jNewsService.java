package com.artamm.newsparser.service;

import com.artamm.newsparser.model.jNews;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface jNewsService {
    List<jNews> getAllNews();

    List<jNews> getAllBySource(String source);

    List<jNews> getAllByTitle(String title);

    List<jNews> getAllByTag(String tag);

    void save(jNews news);

    boolean isExist(String title);

}
