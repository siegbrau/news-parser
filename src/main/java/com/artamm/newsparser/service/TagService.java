package com.artamm.newsparser.service;

import com.artamm.newsparser.model.Tag;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface TagService {
    Tag[] allTagList();

    boolean tagExists(String tag);

    List<Tag> defineTags(String toLowerCase);
}
