package com.artamm.newsparser.service;

import org.springframework.stereotype.Component;


@Component
public interface ParseNews {
    void parseAllNewNews();

}
