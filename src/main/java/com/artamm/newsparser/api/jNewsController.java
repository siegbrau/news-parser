package com.artamm.newsparser.api;

import com.artamm.newsparser.model.jNews;
import com.artamm.newsparser.service.jNewsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class jNewsController {
	private final jNewsService jNewsService;

	@Autowired
	public jNewsController(com.artamm.newsparser.service.jNewsService jNewsService) {
		this.jNewsService = jNewsService;
	}

	@GetMapping("/news/source/all")
	public List<jNews> allNews(){
		return jNewsService.getAllNews();
	}

	@GetMapping("/news/source/{source}")
	public List<jNews> allNewsBySource(@PathVariable String source){
		return jNewsService.getAllBySource(source);
	}

	@GetMapping("/news/topic/{title}")
	public List<jNews> allNewsByTopic(@PathVariable String title){
		return jNewsService.getAllByTitle(title);
	}

	@GetMapping("/news/tag/{title}")
	public List<jNews> allNewsByTag(@PathVariable String title) {
		return jNewsService.getAllByTag(title.toLowerCase());
	}


}
