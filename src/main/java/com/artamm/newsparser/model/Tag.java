package com.artamm.newsparser.model;

public enum Tag {
    games, news, technology, dev
}
