package com.artamm.newsparser.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.CollectionTable;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import java.util.List;

@Entity
@Getter
@Setter
@NoArgsConstructor
public class jNews {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String title;

    private String link;
    @Enumerated(EnumType.STRING)
    private Source source;

    @ElementCollection(fetch = FetchType.EAGER)
    @CollectionTable(name = "Tag", joinColumns = @JoinColumn(name = "id"))
    @Enumerated(EnumType.STRING)
    private List<Tag> tagList;

    public jNews(String title, Source source, String link) {
        this.title = title;
        this.source = source;
        this.link = link;
    }

    public jNews(String title, String link, Source source,
                 List<Tag> tagList) {
        this.title = title;
        this.link = link;
        this.source = source;
        this.tagList = tagList;
    }
}
