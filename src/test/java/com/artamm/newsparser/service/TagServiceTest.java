package com.artamm.newsparser.service;

import com.artamm.newsparser.service.impl.TagServiceImpl;
import org.junit.Assert;
import org.junit.jupiter.api.Test;

import java.util.Map;

public class TagServiceTest {
    private static final TagService tagService = new TagServiceImpl();

//    TODO Use Mockito
    @Test
    public void CheckTagsAreAdded() {
        //        News title/ expected tag number
        Map<String, Integer> newsWithTags = Map.of(
                "ИГРА БЫЛА РАЗРАБОТАНА ОТВЕТСТВЕННЫМИ РАЗРАБОТЧИКАМИ", 2,
                "САМЫЕ ОЖИДАЕМЫЕ ИГРЫ? ELDEN RING ОПЯТЬ НЕ ВЫЙДЕТ", 1,
                "ПОЧЕМУ ГАЗ ПОДОРОЖАЛ: ДЛЯ ЧАЙНИКОВ", 0);

        newsWithTags.forEach((news, number) -> {
            Integer actualTagCount = tagService.defineTags(news.toLowerCase()).size();
            Assert.assertEquals(number, actualTagCount);
        });

    }

}

