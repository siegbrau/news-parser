# news-parser (backend)

## About
Backend part for parsing game and dev news from ycombinator, Stopgame and gametech. Uses Java & Spring, Keycloak.

###Setup
1. Clone project. Setup keycloak as written in their documentation and add realm data to application.properties
2. Run mvn package
3. Run using java -jar or run Dockerfile 
4. Open /swagger-ui.html  and use endpoints or clone newsparser-ui repository and open localhost:4200
<img src="/uploads/6ffdc487c15c4c5c55186f55443f43bd/ss.JPG"   height="400">
